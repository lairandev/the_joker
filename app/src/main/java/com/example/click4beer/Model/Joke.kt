package com.example.click4beer.Model

class Joke (val id: Long, val type: String, val setup: String, val punchline: String)