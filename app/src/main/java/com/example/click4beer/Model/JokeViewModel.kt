package com.example.click4beer.Model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class JokeViewModel: ViewModel() {
    var jokelist = MutableLiveData<MutableList<Joke>>().apply {
        this.value = mutableListOf<Joke>(
            Joke(1, "general", "How does a train eat?", "It goes chew, chew"),
            Joke(2, "programming", "What's the best thing about a Boolean?", "Even if you're wrong, you're only off by a bit.")
        )
    }

    var selectedJoke = MutableLiveData<Joke>()

    fun select(joke: Joke) {
//        comment
        selectedJoke.postValue(joke)
    }
}