package com.example.click4beer

import com.example.click4beer.Model.Category

interface CategoryOnClickListener {
        fun onClick(category: Category)
}