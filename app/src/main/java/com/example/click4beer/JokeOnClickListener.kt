package com.example.click4beer

import com.example.click4beer.Model.Category
import com.example.click4beer.Model.Joke

interface JokeOnClickListener {
        fun onClick(joke: Joke)
}