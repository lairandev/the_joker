package com.example.checkmeapp

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.click4beer.retrofit.ApiInterface
import com.example.click4beer.Model.Category
import com.example.click4beer.Model.Joke
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository {

    private val apiInterface = ApiInterface.create()
    var jokesCategory = MutableLiveData<MutableList<Category>>()
    var joke = MutableLiveData<MutableList<Joke>>()

    fun fetchCategory(url: String): MutableLiveData<MutableList<Category>> {
        val call = apiInterface.getData(url)
        call.enqueue(object : Callback<MutableList<Category>> {
            @SuppressLint("NullSafeMutableLiveData")
            override fun onFailure(call: Call<MutableList<Category>>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
                jokesCategory.postValue(null)
            }

            override fun onResponse(call: Call<MutableList<Category>>,
                response: Response<MutableList<Category>>) {
                if (response != null && response.isSuccessful) {
                    jokesCategory.value = response.body()
                }
            }
        })
        return jokesCategory
    }

    fun fetchJoke(url: String): MutableLiveData<MutableList<Joke>> {
        val call = apiInterface.getJoke(url)
        call.enqueue(object : Callback<MutableList<Joke>> {
            @SuppressLint("NullSafeMutableLiveData")
            override fun onFailure(call: Call<MutableList<Joke>>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
                joke.postValue(null)
            }

            override fun onResponse(call: Call<MutableList<Joke>>,
                                    response: Response<MutableList<Joke>>) {
                if (response != null && response.isSuccessful) {
                    joke.value = response.body()
                }
            }
        })
        return joke
    }
}