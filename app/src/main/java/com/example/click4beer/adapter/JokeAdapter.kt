package com.example.click4beer.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.click4beer.JokeOnClickListener
import com.example.click4beer.Model.Joke
import com.example.click4beer.R
import com.example.click4beer.databinding.ItemCategoryBinding
import com.example.click4beer.databinding.ItemJokeBinding

class JokeAdapter (private val jokes: MutableList<Joke>, private val listenerCategory: JokeOnClickListener):
    RecyclerView.Adapter<JokeAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_joke, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val joke = jokes[position]
        with(holder){
            setListener(joke)
            binding.jokeSetup.text = joke.setup
            binding.jokePunchline.text = joke.punchline
        }
    }

    override fun getItemCount(): Int {
        return jokes.size
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemJokeBinding.bind(view)

        fun setListener(joke: Joke){
            binding.root.setOnClickListener {
                listenerCategory.onClick(joke)
            }
        }
    }
}