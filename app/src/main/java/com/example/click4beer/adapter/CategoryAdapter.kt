package com.example.click4beer.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.click4beer.Model.Category
import com.example.click4beer.CategoryOnClickListener
import com.example.click4beer.R
import com.example.click4beer.databinding.ItemCategoryBinding

class CategoryAdapter (private val categories: MutableList<Category>, private val listenerCategory: CategoryOnClickListener):
    RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_category, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val category = categories[position]
        with(holder){
            setListener(category)
            binding.textViewName.text = category.type
        }
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemCategoryBinding.bind(view)

        fun setListener(category: Category){
            binding.root.setOnClickListener {
                listenerCategory.onClick(category)
            }
        }
    }
}