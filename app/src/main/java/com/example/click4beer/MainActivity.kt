package com.example.click4beer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.click4beer.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)

    }
    
    /*
    Usefull links:
    https://apilist.fun/api/untappd
    https://untappd.com/api/docs?ref=apilist.fun
    https://untappd.com/api/register#
    */

    /*
    #ffcf0d - beer color
    #bc6822 - bottle color
    #82491e - dark bottle color
    */
}