package com.example.click4beer.retrofit

import retrofit2.Call
import com.example.click4beer.Model.Category
import com.example.click4beer.Model.Joke
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Url

interface ApiInterface {

    @GET()
    fun getData(@Url url: String): Call<MutableList<Category>>

    @GET()
    fun getJoke(@Url url: String): Call<MutableList<Joke>>

    companion object {
        val BASE_URL = "https://official-joke-api.appspot.com/"

        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

            return retrofit.create(ApiInterface::class.java)
        }
    }
}
