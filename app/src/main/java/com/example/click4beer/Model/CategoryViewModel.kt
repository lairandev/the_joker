package com.example.click4beer.Model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.checkmeapp.Repository

class CategoryViewModel: ViewModel(){
    val repository = Repository()
    var categories = MutableLiveData<MutableList<Category>>()
    var jokes = MutableLiveData<MutableList<Joke>>()
    var actualCategory = MutableLiveData<Category>()

    init{
        fetchCategory("jokes")
    }

    private fun fetchCategory(url: String) {
        categories = repository.fetchCategory(url)
    }

    fun setTaskList(category: Category, jokes: List<Joke>){
        actualCategory.value = category
        fetchJokes("jokes/"+category.type)
    }

    private fun fetchJokes(url: String) {
        jokes = repository.fetchJoke(url)
    }

    fun setCategory(category: Category){
        actualCategory.value = category
    }
/*
    var categoryList = MutableLiveData<MutableList<Category>>().apply {
        this.value = mutableListOf<Category>(
            Category("general"),
            Category("programming")
        )
    }

    var selectedCategory = MutableLiveData<Category>()

    fun select(category: Category) {
        selectedCategory.postValue(category)
    }*/
}